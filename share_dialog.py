# -*- coding: utf-8 -*-

"""
Share data. Elementary data exchange

Start on 12/10/2020
__author__: Observatoire Pelagis
__copyright__ = "Copyright 2022, Observatoire Pelagis"
__version__ = "2.4"
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
#  Copyright (c) $2022. Observatoire Pelagis

# --------------------------------------------------------------------------------
# ============================= Imports Python ===================================
import csv
from collections import OrderedDict

import processing
import os
import pandas as pd
# ============================= Imports PyQt ===================================
from PyQt5.QtCore import QVariant
# ============================= Imports QGis ===================================
from PyQt5.QtWidgets import QDialog, QWidget, QFileDialog, QMessageBox
from qgis.core import QgsProject, QgsMessageLog, Qgis, QgsVectorLayer, edit, QgsCoordinateReferenceSystem, \
    QgsVectorFileWriter, QgsFeatureRequest, QgsField, QgsUnitTypes, QgsDistanceArea, QgsExpressionContext, QgsExpression
from qgis.utils import iface
# ============================= Imports Modules internes  ======================
from .forms.share_form import *
# --------------------------------------------------------------------------------


class Share_Dialog(QDialog, Ui_Share_Dialog):

    def __init__(self, interface):

        QWidget.__init__(self)
        self.setupUi(self)
        self.interface = interface

        self.working_dir = None  # working directory (save working files)
        self.export_dir = None  # export directory

        self.effort_path = None  # effort file
        self.sigh_path = None  # effort file
        self.linear_path = None  # effort file

        # type of file and expression
        self.chb_dee.setChecked(True)

        self.fill_cb_layers()

        # Boutons de validation
        self.pb_create.clicked.connect(self.run)

        self.export_path = ''  # path of export directory

        self.effort_layer = None
        self.observation_layer = None
        self.linear_layer = None
        self.species_table = None
        self.observer_table = None
        self.survey_table = None
        # self.aerial_obs_select = None  # Observation with status != RECAPTURE
        self.list_lyr = []  # list of layers to group in geopackage

########################################################################################################################

    def run(self):

        """
        Algorithm to launch creation of file
        :return:
        """

        self.pb_create.setStyleSheet("background-color: #E0DAE0")

        self.export_dir = QFileDialog.getExistingDirectory(self, 'Select export directory', "C:/")

        if self.export_dir == '':
            QMessageBox.critical(self, 'Error', 'No directory selected.')

        else:
            self.list_lyr = []

            self.effort_path = self.cb_lyr_eff.currentText()  # couche/chemin sélectionné dans la liste déroulante
            self.sigh_path = self.cb_lyr_sigh.currentText()  # couche/chemin sélectionné dans la liste déroulante
            self.linear_path = self.cb_lyr_linear.currentText()  # couche/chemin sélectionné dans la liste déroulante

            if self.effort_path != 'None':
                self.effort_layer = QgsProject.instance().mapLayersByName(self.effort_path)[0]

            if self.sigh_path != 'None':
                self.observation_layer = QgsProject.instance().mapLayersByName(self.sigh_path)[0]

            if self.linear_path != 'None':
                self.linear_layer = QgsProject.instance().mapLayersByName(self.linear_path)[0]

            root = QgsProject.instance().mapLayers().values()
            for layer in root:
                if layer.name() == 'species':
                    self.species_table = layer
                if layer.name() == 'observer':
                    self.observer_table = layer
                if layer.name() == 'survey':
                    self.survey_table = layer

            # -------EFFORT
            if self.effort_layer:
                if self.rb_aerial.isChecked():
                    QgsMessageLog.logMessage('Processing aerial survey effort.')

                    dc_fields_eff = ['fid', 'region', 'survey',  'cycle', 'session', 'subRegion', 'strate', 'strateType',
                                   'transect', 'flight', 'computer', 'routeType', 'effortGrp',
                                   'effort', 'status', 'date', 'hhmmss', 'seaState', 'swell', 'turbidity',
                                   'skyGlint', 'glareFrom', 'glareTo', 'glareSever', 'glareUnder',
                                   'cloudCover', 'subjective', 'unexpLeft', 'unexpRight', 'left', 'right',
                                   'center', 'cbCause', 'lat', 'lon', 'speed', 'altitude', 'gpsDelay',
                                   'aircraft', 'comment', 'datetime', 'nom_suivi','credit', 'financeur', 'unique_id']

                    dee_fields_eff = ['fid', 'region', 'survey', 'cycle', 'session', 'subRegion', 'strate', 'transect', 'effort',
                                    'computer', 'routeType', 'status', 'date', 'hhmmss', 'seaState',
                                    'subjective', 'lat', 'lon', 'datetime', 'nom_suivi','credit', 'financeur', 'unique_id']

                    suffix = 'aerial'

                else:
                    QgsMessageLog.logMessage('Processing ship survey effort.')

                    dc_fields_eff = ['fid', 'region', 'survey', 'computer', 'routeType', 'effortGrp', 'effort', 'status',
                                   'date', 'hhmmss', 'seaState', 'swell', 'glareFrom', 'glareto', 'glareSever', 'cloudCover',
                                   'subjective', 'left', 'right', 'lat', 'lon', 'speed', 'comment',
                                   'vent_vrai_direction', 'vent_vrai_force', 'houle_direction', 'houle_hauteur',
                                   'visibilite', 'n_observateurs', 'plateforme', 'code_trans', 'cap', 'nom_suivi', 'credit', 'financeur', 'unique_id']

                    dee_fields_eff = ['fid', 'region', 'survey', 'effort', 'computer', 'routeType', 'status', 'date',
                                    'hhmmss', 'seaState', 'subjective', 'lat', 'lon', 'datetime', 'nom_suivi','credit', 'financeur', 'unique_id']

                    suffix = 'boat'

                if self.chb_dc.isChecked():
                    self.select_effort(self.effort_layer, 'dc_effort_'+suffix, dc_fields_eff)
                if self.chb_dee.isChecked():  # dee
                    self.select_effort(self.effort_layer, 'dee_effort_'+suffix, dee_fields_eff)

            # ----------------- EFFORT LINEARISE
            if self.linear_layer:
                QgsMessageLog.logMessage('Processing linear effort.')

                dc_fields_lin = ['fid', 'region', 'survey', 'subRegion', 'cycle', 'session', 'routeType',
                               'strate', 'strateType', 'samplPlan', 'computer', 'transect', 'flight',
                               'effortGrp', 'effort', 'status', 'date', 'hhmmss', 'seaState', 'swell',
                               'turbidity', 'skyGlint', 'glareFrom', 'glareTo', 'glareSever',
                               'glareUnder', 'cloudCover', 'subjective', 'unexpLeft', 'unexpRight',
                               'left', 'right', 'center', 'cbCause', 'lat', 'lon', 'speed', 'altitude',
                               'gpsDelay', 'aircraft', 'comment', 'datetime', 'DATE_TIME1', 'DATE_TIME2',
                               'legID', 'join_lid', 'legLengKm', 'dTimeMin', 'seaStIndex', 'subjNumL',
                               'subjNumR', 'nom_suivi']

                dee_fields_lin = ['fid', 'region', 'survey',  'cycle', 'session', 'subRegion', 'routeType', 'strate', 'computer',
                                'transect', 'effort', 'status', 'date', 'hhmmss', 'seaState',
                                'subjective', 'lat', 'lon', 'datetime',
                                'DATE_TIME1', 'DATE_TIME2', 'legLengKm', 'nom_suivi']

                if self.chb_dc.isChecked():
                    self.select_effort(self.linear_layer, 'dc_' + self.linear_layer.name(), dc_fields_lin)
                if self.chb_dee.isChecked():  # dee
                    self.select_effort(self.linear_layer, 'dee_' + self.linear_layer.name(), dee_fields_lin)

            # ----------------- OBSERVATION

            if self.observation_layer:

                # DEE
                if self.chb_dee.isChecked():
                    fields_obs = ['fid', 'region', 'survey', 'subRegion', 'cycle', 'session', 'computer', "routeType", 'sighting',
                                     'date', 'hhmmss', 'species', 'taxon_fr', 'famille_fr', 'groupe_fr', 'nom_fr',
                                     'nom_latin', 'taxon_eng', 'family_eng', 'group_eng', 'name_eng', 'cd_nom', 'cd_taxsup',
                                     "podSize", 'lat', 'lon', 'nom_suivi','credit', 'financeur', 'unique_id']
                    prefix = 'dee'

                    if self.rb_boat.isChecked():
                        QgsMessageLog.logMessage('Processing observation boat table.')
                        self.select_observation(self.observation_layer, prefix + '_observation_boat', fields_obs)

                    else:
                        QgsMessageLog.logMessage('Processing observation aerial table.')

                        # enlever les RECAPTURES
                        self.observation_layer.selectByExpression(''' "status"!='RECAPTURE' ''',
                                                                  QgsVectorLayer.SetSelection)
                        algo_save_selection = processing.run(
                            'qgis:saveselectedfeatures', {
                                'INPUT': self.observation_layer, 'OUTPUT': 'TEMPORARY_OUTPUT'})
                        self.aerial_obs_select = algo_save_selection['OUTPUT']
                        self.observation_layer.removeSelection()

                        self.select_observation(self.aerial_obs_select, prefix + '_observation_aerial', fields_obs)

                # COMPLETE DATA
                if self.chb_dc.isChecked():
                    fields_obs = ['fid', 'region', 'survey', 'subRegion', 'cycle', 'session', 'strateType', 'strate',
                                    'transect', 'flight', 'computer', 'routeType', 'effortGrp', 'effort',
                                    'sighting', 'date', 'hhmmss', 'species', 'taxon_fr', 'famille_fr', 'groupe_fr',
                                    'nom_fr', 'nom_latin', 'taxon_eng', 'family_eng', 'group_eng', 'name_eng', 'cd_nom',
                                    'cd_taxsup', 'podSize', 'age', 'decAngle', 'cue', 'behaviour', 'swimDir', 'calves',
                                    'photo', 'observer', 'prenom', 'nom', 'side', 'status', 'cbCause', 'lat', 'lon',
                                    'speed', 'altitude',
                                    'gpsDelay', 'aircraft', 'distance', 'min', 'max', 'groupBehav', 'malchance',
                                    'associatio', 'cptmt_mm', 'cptmt_ois', 'cptmt_bat' 'comment', 'datehhmmss',
                                    'legID', 'join_lid', 'perpDist', 'pDistTmp','nom_suivi', 'credit', 'financeur', 'unique_id']  # fields for aerial survey AND boat survey
                    prefix = 'dc'

                    if self.rb_boat.isChecked():
                        QgsMessageLog.logMessage('Processing observation boat table.')

                        self.select_observation(self.observation_layer, prefix + '_observation_boat', fields_obs)

                    else:
                        QgsMessageLog.logMessage('Processing observation aerial table.')

                        # enlever les RECAPTURES
                        self.observation_layer.selectByExpression(''' "status"!='RECAPTURE' ''',
                                                                  QgsVectorLayer.SetSelection)
                        algo_norecapt = processing.run(
                            'qgis:saveselectedfeatures', {
                                'INPUT': self.observation_layer, 'OUTPUT': 'TEMPORARY_OUTPUT'})
                        self.aerial_obs_select = algo_norecapt['OUTPUT']
                        self.observation_layer.removeSelection()

                        self.select_observation(self.aerial_obs_select, prefix + '_observation_aerial', fields_obs)

            self.interface.messageBar().pushMessage("Success", "The formatting of the exchange data is done.", level=Qgis.Success)
            self.pb_create.setStyleSheet("background-color: #A9FF99")
            # QMessageBox.information(None, "Information", 'The formatting of the exchange data is done.')

########################################################################################################################
# ----------- FILL COMBOBOX

    def fill_cb_layers(self):
        """
        Fill comboBox with layer open in QGis session
        :return: lst_lyr
        """

        self.cb_lyr_eff.clear()
        self.cb_lyr_sigh.clear()
        self.cb_lyr_linear.clear()

        lst_lyr = []
        self.root = QgsProject.instance().layerTreeRoot()
        for layer in self.root.findLayers():
            lst_lyr.append(layer.name())

        self.cb_lyr_sigh.addItems(lst_lyr)
        self.cb_lyr_eff.addItems(lst_lyr)
        self.cb_lyr_linear.addItems(lst_lyr)

        self.cb_lyr_sigh.insertItem(0, "None")
        self.cb_lyr_eff.insertItem(0, "None")
        self.cb_lyr_linear.insertItem(0, "None")

        self.cb_lyr_sigh.setCurrentIndex(0)
        self.cb_lyr_eff.setCurrentIndex(0)
        self.cb_lyr_linear.setCurrentIndex(0)

        return lst_lyr

# ----------------- SELECT EFFORT COLUMNS
    def select_effort(self, layer, layer_name, fld2keep):
        """
        Select effort column. boat or aerial.
        :return:
        """

        layer.selectByExpression(''' "routeType"='prospection' or "routeType"='LEG' ''')
        algo_saverT = processing.run('qgis:saveselectedfeatures', {
            'INPUT': layer, 'OUTPUT': 'TEMPORARY_OUTPUT'})

        effort_filter = algo_saverT['OUTPUT']
        layer.removeSelection()

        # jointure avec table survey
        if self.survey_table:
            QgsMessageLog.logMessage('Join effort table with survey table.')

            effort_filter.addExpressionField(
                ''' CONCAT(survey, '_', cycle,'_',session,'_',subRegion) ''',
                QgsField('jointure', QVariant.String))
            effort_filter.updateFields()

            self.survey_table.addExpressionField(
                ''' CONCAT(survey, '_', cycle,'_',session,'_',subRegion) ''',
                QgsField('jointure', QVariant.String))
            self.survey_table.updateFields()

            algo_join_survey = processing.run("native:joinattributestable", {
                'INPUT': effort_filter,
                'FIELD': 'jointure',
                'INPUT_2': self.survey_table,
                'FIELD_2': 'jointure',
                'FIELDS_TO_COPY': ['credit', 'financeur'], 'METHOD': 1,
                'DISCARD_NONMATCHING': False, 'PREFIX': '', 'OUTPUT': 'TEMPORARY_OUTPUT'})
            layer2export = algo_join_survey['OUTPUT']

            # EXPORT METADATA in txt from survey table

            # get unique values in jointure from effort file
            idx_join = layer2export.fields().indexFromName('jointure')
            uniqueV = layer2export.uniqueValues(idx_join)
            QgsMessageLog.logMessage('Survey: {0}'.format(uniqueV))

            # create export file
            path_metadata = os.path.join(self.export_dir, 'metadata.csv')
            
            for value in uniqueV:
                self.survey_table.selectByExpression(''' "jointure" = '{0}' '''.format(value), QgsVectorLayer.AddToSelection)

            metadata_filtered = self.survey_table.materialize(QgsFeatureRequest().setFilterFids(self.survey_table.selectedFeatureIds()))

            # reordonner les colonnes
            algo_refactor = processing.run('qgis:refactorfields',
                           {'INPUT': metadata_filtered,
                            'FIELDS_MAPPING':[{'expression': '\"credit\"','length': 0,'name': 'credit','precision': 0,'type': 10},
                                              {'expression': '\"surveyLong\"','length': 0,'name': 'surveyLong','precision': 0,'type': 10},
                                              {'expression': '\"surveyName\"','length': 0,'name': 'surveyName','precision': 0,'type': 10},
                                              {'expression': '\"survey\"','length': 0,'name': 'survey','precision': 0,'type': 10},
                                              {'expression': '\"stockage\"','length': 0,'name': 'stockage','precision': 0,'type': 10},
                                              {'expression': '\"regionLong\"','length': 0,'name': 'regionLong','precision': 0,'type': 10},
                                              {'expression': '\"region\"','length': 0,'name': 'region','precision': 0,'type': 10},
                                              {'expression': '\"subRegion\"','length': 0,'name': 'subRegion','precision': 0,'type': 10},
                                              {'expression': '\"srm\"','length': 0,'name': 'srm','precision': 0,'type': 10},
                                              {'expression': '\"cycle\"','length': 0,'name': 'cycle','precision': 0,'type': 10},
                                              {'expression': '\"platform\"','length': 0,'name': 'platform','precision': 0,'type': 10},
                                              {'expression': '\"typePlatform\"','length': 0,'name': 'typePlatform','precision': 0,'type': 10},
                                              {'expression': '\"doublePlatform\"','length': 0,'name': 'doublePlatform','precision': 0,'type': 10},
                                              {'expression': '\"protocole\"','length': 0,'name': 'protocole','precision': 0,'type': 10},
                                              {'expression': '\"vitesse\"','length': 0,'name': 'vitesse','precision': 0,'type': 10},
                                              {'expression': '\"altitude\"','length': 0,'name': 'altitude','precision': 0,'type': 10},
                                              {'expression': '\"systemeHoraire\"','length': 0,'name': 'systemeHoraire','precision': 0,'type': 10},
                                              {'expression': '\"convention\"','length': 0,'name': 'convention','precision': 0,'type': 10},
                                              {'expression': '\"financeur\"','length': 0,'name': 'financeur','precision': 0,'type': 10},
                                              {'expression': '\"gestionnaire\"','length': 0,'name': 'gestionnaire','precision': 0,'type': 10}],
                            'OUTPUT': path_metadata})

            # enlever les duplicats entre cycle d'une même campagne
            metadata_pd = pd.read_csv(path_metadata)
            metadata_pd.drop_duplicates(inplace=True)
            metadata_pd.to_csv(path_metadata, index=False)

        else:
            layer2export = effort_filter
            self.interface.messageBar().pushMessage("Warning",
                                                    "Unable to join effort with the survey table: "
                                                    "survey table missing",
                                                    level=Qgis.Warning)
        ##
        # ajout colonnes nom_suivi
        with edit(layer2export):
            layer2export.dataProvider().addAttributes([QgsField('nom_suivi', QVariant.String)])

            field_ids = []
            # Fieldnames to remove
            for field in layer2export.fields():
                if field.name() not in fld2keep:
                    field_ids.append(layer2export.fields().indexFromName(field.name()))
            # Delete fields
            layer2export.dataProvider().deleteAttributes(field_ids)
            layer2export.updateFields()

            exp_nomsuivi = QgsExpression('''CONCAT(survey, '_', cycle,'(',session,')')''')

            for feat in layer2export.getFeatures():
                context = QgsExpressionContext()
                context.setFeature(feat)

                idxns = layer2export.fields().indexFromName('nom_suivi')
                feat[idxns] = exp_nomsuivi.evaluate(context)
                layer2export.updateFeature(feat)

        # export de la couche effort finale
        path = os.path.join(self.export_dir, layer_name+'.shp')
        save_options = QgsVectorFileWriter.SaveVectorOptions()
        save_options.driverName = "ESRI Shapefile"
        save_options.fileEncoding = "UTF-8"
        transform_context = QgsProject.instance().transformContext()

        QgsMessageLog.logMessage('path: {0}'.format(path))
        _writer, error_string = QgsVectorFileWriter.writeAsVectorFormatV2(
            layer2export, path, transform_context, save_options)

        if _writer == QgsVectorFileWriter.NoError:
            QgsMessageLog.logMessage('%s layer export completed.' % layer.name())
            effort_lyr_saved = iface.addVectorLayer(path, layer_name, "ogr")
            self.list_lyr.append(effort_lyr_saved.id())
        else:
            QgsMessageLog.logMessage('{0} layer is not valid: {1}'.format(layer.name(), error_string))

    # ----------------- SELECT OBSERVATION COLUMNS

    def select_observation(self, layer, layer_name, fld2keep):
        """
        Select observation columns and add new columns. boat or aerial.
        :return:
        """

        layer2export = None
        QgsMessageLog.logMessage('No filter on observation')
        layer.selectAll()
        algo_select = processing.run('qgis:saveselectedfeatures', {'INPUT': layer, 'OUTPUT': 'TEMPORARY_OUTPUT'})

        obs_filter = algo_select['OUTPUT']
        layer.removeSelection()

        # jointure avec table espèce si ouvert dans la session
        if self.species_table:
            QgsMessageLog.logMessage('Join observation table with species table.')

            algo_join_sps = processing.run("native:joinattributestable", {
                'INPUT': obs_filter,
                'FIELD': 'species',
                'INPUT_2': self.species_table,
                'FIELD_2': 'code',
                'FIELDS_TO_COPY': ['taxon_fr', 'famille_fr', 'groupe_fr', 'nom_fr', 'nom_latin', 'taxon_eng',
                                   'family_eng', 'group_eng', 'name_eng', 'cd_nom', 'cd_taxsup'], 'METHOD': 1,
                'DISCARD_NONMATCHING': False, 'PREFIX': '', 'OUTPUT': 'TEMPORARY_OUTPUT'})

            layer_join = algo_join_sps['OUTPUT']

        else:
            self.interface.messageBar().pushMessage("Warning", "Unable to join observations with the species table: "
                                                               "species table missing", level=Qgis.Warning)

            layer_join = obs_filter

        # jointure avec table survey si ouvert dans la session
        if self.survey_table:
            QgsMessageLog.logMessage('Join observation table with survey table.')

            layer_join.addExpressionField(
                ''' CONCAT(survey, '_', cycle,'_',session,'_',subRegion) ''',
                QgsField('jointure', QVariant.String))
            layer_join.updateFields()

            self.survey_table.addExpressionField(
                ''' CONCAT(survey, '_', cycle,'_',session,'_',subRegion) ''',
                QgsField('join_obs', QVariant.String))
            self.survey_table.updateFields()

            algo_join_survey = processing.run("native:joinattributestable", {
                'INPUT': layer_join,
                'FIELD': 'jointure',
                'INPUT_2': self.survey_table,
                'FIELD_2': 'join_obs',
                'FIELDS_TO_COPY': ['credit', 'financeur'], 'METHOD': 1,
                'DISCARD_NONMATCHING': False, 'PREFIX': '', 'OUTPUT': 'TEMPORARY_OUTPUT'})

            layer2export = algo_join_survey['OUTPUT']

        else:
            layer2export = layer_join
            self.interface.messageBar().pushMessage("Warning",
                                                    "Unable to join observations with the survey table: "
                                                    "survey table missing",
                                                    level=Qgis.Warning)

    # ONLY FOR COMPLETE DC

        if self.chb_dc.isChecked():
            # jointure avec table observateur si ouvert dans la session
            if self.observer_table and self.rb_aerial.isChecked():
                QgsMessageLog.logMessage('Join observation table with observer table.')

                algo_join_obser = processing.run("native:joinattributestable", {
                    'INPUT': layer2export,
                    'FIELD': 'observer',
                    'INPUT_2': self.observer_table,
                    'FIELD_2': 'code',
                    'FIELDS_TO_COPY': ['prenom', 'nom'], 'METHOD': 1,
                    'DISCARD_NONMATCHING': False, 'PREFIX': '', 'OUTPUT': 'TEMPORARY_OUTPUT'})

                layer2export = algo_join_obser['OUTPUT']

            else:
                self.interface.messageBar().pushMessage("Warning",
                                                        "Unable to join observations with the observer table: "
                                                        "observer table missing",
                                                        level=Qgis.Warning)

            attr_sigh = []
            for field in layer2export.fields():
                attr_sigh.append(field.name())

            # suppression du champ perpDist si existant
            if 'perpDist' in attr_sigh:
                with edit(layer2export):
                    idxpD = layer2export.fields().indexFromName("perpDist")
                    layer2export.dataProvider().deleteAttributes([idxpD])
                    layer2export.updateFields()
                    QgsMessageLog.logMessage('perpDist attribute has been deleted.')

            # création du champ pDistTmp
            layer2export.dataProvider().addAttributes(
                [QgsField('pDistTmp', QVariant.Double, 'double', 5, 2)])
            layer2export.updateFields()

            exp_perpD_aerial = QgsExpression(''' CASE WHEN 
                                                                "famille_fr"!='Navire' and ("decAngle" = 1 or "decAngle" = 3) THEN 200
                                                                WHEN "famille_fr"!='Navire' and "decAngle" = 2  THEN 500
                                                                WHEN "famille_fr"!='Navire' and "decAngle" = 0 THEN -9999 
                                                                WHEN "famille_fr"='Navire' and "decAngle" = 1 THEN 500
                                                                WHEN "famille_fr"='Navire' and "decAngle" = 2 THEN 600
                                                                ELSE 183*tan(0.01745*(90-"decAngle"))
                                                                END ''')

            exp_perpD_boat = QgsExpression(''' abs(sin("decAngle"*(pi()/180)))*"distance" ''')

            for feat in layer2export.getFeatures():
                context = QgsExpressionContext()
                context.setFeature(feat)

                idxp = layer2export.fields().indexFromName('pDistTmp')
                with edit(layer2export):
                    if self.rb_aerial.isChecked():
                        if 'famille_fr' in attr_sigh:
                            feat[idxp] = exp_perpD_aerial.evaluate(context)
                            layer2export.updateFeature(feat)
                    else:
                        feat[idxp] = exp_perpD_boat.evaluate(context)
                        layer2export.updateFeature(feat)

        # export de la couche finale
        path = os.path.join(self.export_dir, layer_name + '.shp')
        save_options = QgsVectorFileWriter.SaveVectorOptions()
        save_options.driverName = "ESRI Shapefile"
        save_options.fileEncoding = "UTF-8"
        transform_context = QgsProject.instance().transformContext()

        QgsMessageLog.logMessage('path: {0}'.format(path))
        _writer, error_string = QgsVectorFileWriter.writeAsVectorFormatV2(
            layer2export, path, transform_context, save_options)

        if _writer == QgsVectorFileWriter.NoError:
            QgsMessageLog.logMessage('%s layer export completed.' % layer.name())
            obs_lyr_saved = iface.addVectorLayer(path, layer_name, "ogr")

            # ajout de la colonne nom_suivi
            obs_lyr_saved.addExpressionField(
                ''' CONCAT(survey, '_', cycle,'(',session,')') ''',
                QgsField('nom_suivi', QVariant.String))

            with edit(obs_lyr_saved):

                field_ids = []
                # Fieldnames to remove
                for field in obs_lyr_saved.fields():
                    if field.name() not in fld2keep:
                        field_ids.append(obs_lyr_saved.fields().indexFromName(field.name()))

                # Delete fields
                obs_lyr_saved.dataProvider().deleteAttributes(field_ids)
                obs_lyr_saved.updateFields()

            self.list_lyr.append(obs_lyr_saved.id())

        else:
            QgsMessageLog.logMessage('{0} layer is not valid: {1}'.format(layer.name(), error_string))

# ----------------------- EXPORT METADATA

    def export_metadata(self, list_metadata):
        """
        Export txt file with metadata from survey table
        :return:
        """
        path = QFileDialog.getSaveFileName(self, 'Save metadata', self.export_dir, 'TXT(*.txt)')

        if path:
            f = open(path[0], 'w', newline='')
            for i, elm in enumerate(list_metadata):
                f.write('{0} \n'.format(list_metadata[i]))
            f.close()
        else:
            QMessageBox.critical(self, 'Error',
                                 'No txt file created.')
