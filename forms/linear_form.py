# -*- coding: utf-8 -*-

#  Copyright (c) $Manon Niviere, Sophie Laran, Oriane Penot, Adrien Gatineau, Alain Layec. 2020. Observatoire Pelagis

# Form implementation generated from reading ui file 'linear_form.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_linear(object):
    def setupUi(self, linear):
        linear.setObjectName("linear")
        linear.resize(901, 304)
        linear.setMinimumSize(QtCore.QSize(650, 0))
        self.verticalLayout = QtWidgets.QVBoxLayout(linear)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.rb_aerial = QtWidgets.QRadioButton(linear)
        self.rb_aerial.setObjectName("rb_aerial")
        self.horizontalLayout_4.addWidget(self.rb_aerial)
        self.rb_boat = QtWidgets.QRadioButton(linear)
        self.rb_boat.setObjectName("rb_boat")
        self.horizontalLayout_4.addWidget(self.rb_boat)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.lbl_export = QtWidgets.QLabel(linear)
        self.lbl_export.setMinimumSize(QtCore.QSize(120, 0))
        self.lbl_export.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.lbl_export.setObjectName("lbl_export")
        self.horizontalLayout_3.addWidget(self.lbl_export)
        self.le_export = QtWidgets.QLineEdit(linear)
        self.le_export.setMaximumSize(QtCore.QSize(16777215, 25))
        self.le_export.setObjectName("le_export")
        self.horizontalLayout_3.addWidget(self.le_export)
        self.pb_export_dir = QtWidgets.QPushButton(linear)
        self.pb_export_dir.setMaximumSize(QtCore.QSize(16777215, 30))
        self.pb_export_dir.setObjectName("pb_export_dir")
        self.horizontalLayout_3.addWidget(self.pb_export_dir)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.lbl_select_trans = QtWidgets.QLabel(linear)
        self.lbl_select_trans.setMinimumSize(QtCore.QSize(120, 0))
        self.lbl_select_trans.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.lbl_select_trans.setObjectName("lbl_select_trans")
        self.horizontalLayout.addWidget(self.lbl_select_trans)
        self.cb_lyr_eff = QtWidgets.QComboBox(linear)
        self.cb_lyr_eff.setMinimumSize(QtCore.QSize(650, 0))
        self.cb_lyr_eff.setObjectName("cb_lyr_eff")
        self.horizontalLayout.addWidget(self.cb_lyr_eff)
        self.pb_effort_file = QtWidgets.QPushButton(linear)
        self.pb_effort_file.setMaximumSize(QtCore.QSize(16777215, 30))
        self.pb_effort_file.setObjectName("pb_effort_file")
        self.horizontalLayout.addWidget(self.pb_effort_file)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(linear)
        self.label.setMinimumSize(QtCore.QSize(120, 0))
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.cb_lyr_sigh = QtWidgets.QComboBox(linear)
        self.cb_lyr_sigh.setMinimumSize(QtCore.QSize(650, 0))
        self.cb_lyr_sigh.setObjectName("cb_lyr_sigh")
        self.horizontalLayout_2.addWidget(self.cb_lyr_sigh)
        self.pb_sigh_file = QtWidgets.QPushButton(linear)
        self.pb_sigh_file.setMaximumSize(QtCore.QSize(16777215, 30))
        self.pb_sigh_file.setObjectName("pb_sigh_file")
        self.horizontalLayout_2.addWidget(self.pb_sigh_file)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.pb_run = QtWidgets.QPushButton(linear)
        self.pb_run.setObjectName("pb_run")
        self.verticalLayout.addWidget(self.pb_run)

        self.retranslateUi(linear)
        QtCore.QMetaObject.connectSlotsByName(linear)

    def retranslateUi(self, linear):
        _translate = QtCore.QCoreApplication.translate
        linear.setWindowTitle(_translate("linear", "Linearization"))
        self.rb_aerial.setText(_translate("linear", "Aerial survey"))
        self.rb_boat.setText(_translate("linear", "Boat survey"))
        self.lbl_export.setText(_translate("linear", "Export directory"))
        self.pb_export_dir.setText(_translate("linear", "..."))
        self.lbl_select_trans.setText(_translate("linear", "Select effort file"))
        self.pb_effort_file.setText(_translate("linear", "..."))
        self.label.setText(_translate("linear", "Select sighting file"))
        self.pb_sigh_file.setText(_translate("linear", "..."))
        self.pb_run.setText(_translate("linear", "Run"))

