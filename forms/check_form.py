# -*- coding: utf-8 -*-

#  Copyright (c) $Manon Niviere, Sophie Laran, Oriane Penot, Adrien Gatineau, Alain Layec. 2020. Observatoire Pelagis

# Form implementation generated from reading ui file 'check_form.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_check_dialog(object):
    def setupUi(self, check_dialog):
        check_dialog.setObjectName("check_dialog")
        check_dialog.resize(740, 155)
        check_dialog.setAutoFillBackground(False)
        self.verticalLayout = QtWidgets.QVBoxLayout(check_dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label = QtWidgets.QLabel(check_dialog)
        self.label.setMinimumSize(QtCore.QSize(150, 0))
        self.label.setMaximumSize(QtCore.QSize(200, 16777215))
        self.label.setObjectName("label")
        self.horizontalLayout_3.addWidget(self.label)
        self.lbl_effort_path = QtWidgets.QLabel(check_dialog)
        self.lbl_effort_path.setMinimumSize(QtCore.QSize(300, 0))
        self.lbl_effort_path.setMaximumSize(QtCore.QSize(16777215, 30))
        self.lbl_effort_path.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.lbl_effort_path.setFrameShadow(QtWidgets.QFrame.Plain)
        self.lbl_effort_path.setLineWidth(1)
        self.lbl_effort_path.setText("")
        self.lbl_effort_path.setObjectName("lbl_effort_path")
        self.horizontalLayout_3.addWidget(self.lbl_effort_path)
        self.pb_get_effort = QtWidgets.QPushButton(check_dialog)
        self.pb_get_effort.setMaximumSize(QtCore.QSize(50, 16777215))
        self.pb_get_effort.setObjectName("pb_get_effort")
        self.horizontalLayout_3.addWidget(self.pb_get_effort)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_2 = QtWidgets.QLabel(check_dialog)
        self.label_2.setMinimumSize(QtCore.QSize(150, 0))
        self.label_2.setMaximumSize(QtCore.QSize(200, 16777215))
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        self.lbl_obs_path = QtWidgets.QLabel(check_dialog)
        self.lbl_obs_path.setMinimumSize(QtCore.QSize(300, 0))
        self.lbl_obs_path.setMaximumSize(QtCore.QSize(16777215, 30))
        self.lbl_obs_path.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.lbl_obs_path.setText("")
        self.lbl_obs_path.setObjectName("lbl_obs_path")
        self.horizontalLayout_2.addWidget(self.lbl_obs_path)
        self.pb_get_obs = QtWidgets.QPushButton(check_dialog)
        self.pb_get_obs.setMaximumSize(QtCore.QSize(50, 16777215))
        self.pb_get_obs.setObjectName("pb_get_obs")
        self.horizontalLayout_2.addWidget(self.pb_get_obs)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.pb_run = QtWidgets.QPushButton(check_dialog)
        self.pb_run.setObjectName("pb_run")
        self.verticalLayout.addWidget(self.pb_run)
        self.progressBar = QtWidgets.QProgressBar(check_dialog)
        self.progressBar.setProperty("value", 24)
        self.progressBar.setObjectName("progressBar")
        self.verticalLayout.addWidget(self.progressBar)

        self.retranslateUi(check_dialog)
        QtCore.QMetaObject.connectSlotsByName(check_dialog)

    def retranslateUi(self, check_dialog):
        _translate = QtCore.QCoreApplication.translate
        check_dialog.setWindowTitle(_translate("check_dialog", "File checking"))
        self.label.setText(_translate("check_dialog", "Select your effort file :"))
        self.pb_get_effort.setText(_translate("check_dialog", "..."))
        self.label_2.setText(_translate("check_dialog", "Select your observation file :"))
        self.pb_get_obs.setText(_translate("check_dialog", "..."))
        self.pb_run.setText(_translate("check_dialog", "Run"))

