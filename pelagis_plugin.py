# -*- coding: utf-8 -*-
"""
Launch plugin.

__copyright__ = "Copyright 2022, Observatoire Pelagis"
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

#  Copyright (c) $2022. Observatoire Pelagis

# --------------------------------------------------------------------------------
# ============================= Imports PyQt ===================================
from PyQt5.QtWidgets import QAction, QMenu
from PyQt5.QtGui import QIcon
# ============================= Imports QGis ===================================
from qgis.utils import showPluginHelp, iface
# ============================= Imports Modules internes ===================================
from .prepare_file_dialog import *
from .import_dialog import *
from .analyse_dialog import *
from .map_dialog import *
from .linear_dialog import *
from .check_dialog import *
from .segment_dialog import *
from .share_dialog import *
# --------------------------------------------------------------------------------


class PelagisPlugin(object):

    def __init__(self, iface):

        self.interface = iface

        self.dicoFonction = {}

        self.fct_prepare = None
        self.fct_check = None
        self.fct_linear = None
        self.fct_segment = None
        self.fct_analyse = None
        self.fct_map = None
        self.fct_integration = None

        self.menuPelagis = None
        self.toolbarPelagis = None
        self.pluginEstActif = None

    def initGui(self):

        # la clé du dictionnaire est le nom de la fonction
        # 1er paramètre de la liste : si la fenêtre est active ou non
        # 2ème paramètre de la liste : la fenêtre
        # 3ème paramètre de la liste : si la fenêtre est dockée ou non

        self.dicoFonction = {"fct_info": [False, None, False],
                             "fct_prepare": [False, None, False],
                             "fct_check": [False, None, False],
                             "fct_integration": [False, None, False],
                             "fct_linear": [False, None, False],
                             "fct_segment": [False, None, False],
                             "fct_analyse": [False, None, False],
                             "fct_map": [False, None, False],
                             "fct_share": [False, None, False]}

        # FONCTION INFO
        iconfctInfo = QIcon(os.path.dirname(__file__) + "/icons/logo_i.png")
        self.fct_info = QAction(iconfctInfo, "Help", self.interface.mainWindow())
        self.fct_info.triggered.connect(self.help)

        # FONCTION PREPARE
        iconfctPrepare = QIcon(os.path.dirname(__file__) + "/icons/logo_file_sammoa.png")
        self.fct_prepare = QAction(iconfctPrepare, "Prepare files for SAMMOA", self.interface.mainWindow())
        self.fct_prepare.triggered.connect(lambda: self.gereFonction("fct_prepare"))

        # FONCTION CHECK
        iconfctCheck = QIcon(os.path.dirname(__file__) + "/icons/logo_check.png")
        self.fct_check = QAction(iconfctCheck, "Sammoa files checking", self.interface.mainWindow())
        self.fct_check.triggered.connect(lambda: self.gereFonction("fct_check"))

        # FONCTION LINEARISATION
        iconfctLinear = QIcon(os.path.dirname(__file__) + "/icons/logo_linearisation.png")
        self.fct_linear = QAction(iconfctLinear, "Linearize effort", self.interface.mainWindow())
        self.fct_linear.triggered.connect(lambda: self.gereFonction("fct_linear"))

        # FONCTION SEGMENTATION
        iconfctSegment = QIcon(os.path.dirname(__file__) + "/icons/logo_segment.png")
        self.fct_segment = QAction(iconfctSegment, "Segment effort", self.interface.mainWindow())
        self.fct_segment.triggered.connect(lambda: self.gereFonction("fct_segment"))

        # FONCTION INTEGRATION
        iconfctIntegration = QIcon(os.path.dirname(__file__) + "/icons/logo_import_BDD.png")
        self.fct_integration = QAction(iconfctIntegration, "Import into database", self.interface.mainWindow())
        self.fct_integration.triggered.connect(lambda: self.gereFonction("fct_integration"))

        # FONCTION ANALYSE
        iconfctAnalyse = QIcon(os.path.dirname(__file__) + "/icons/logo_analyse.png")
        self.fct_analyse = QAction(iconfctAnalyse, "Analyze", self.interface.mainWindow())
        self.fct_analyse.triggered.connect(lambda: self.gereFonction("fct_analyse"))

        # FONCTION MAP
        iconfctMap = QIcon(os.path.dirname(__file__) + "/icons/logo_atlas.png")
        self.fct_map = QAction(iconfctMap, "Mapping", self.interface.mainWindow())
        self.fct_map.triggered.connect(lambda: self.gereFonction("fct_map"))

        # FONCTION SHARE
        iconfctShare = QIcon(os.path.dirname(__file__) + "/icons/logo_share.png")
        self.fct_share = QAction(iconfctShare, "Share", self.interface.mainWindow())
        self.fct_share.triggered.connect(lambda: self.gereFonction("fct_share"))

        # MENU
        self.menuPelagis = QMenu("pelaSIG")
        self.menuPelagis.addAction(self.fct_info)
        self.menuPelagis.addAction(self.fct_prepare)
        self.menuPelagis.addAction(self.fct_check)
        self.menuPelagis.addAction(self.fct_linear)
        self.menuPelagis.addAction(self.fct_segment)
        self.menuPelagis.addAction(self.fct_analyse)
        self.menuPelagis.addAction(self.fct_map)
        self.menuPelagis.addAction(self.fct_integration)
        self.menuPelagis.addAction(self.fct_share)

        self.interface.mainWindow().menuBar().insertMenu(self.interface.firstRightStandardMenu().menuAction(), self.menuPelagis)

        # BARRE D'OUTILS
        self.toolbarPelagis = self.interface.addToolBar("pelaSIG toolbar")
        self.toolbarPelagis.setObjectName("barreOutilPelagis")

        icon = QIcon(os.path.dirname(__file__) + "/icons/logo_i.png")
        text = "Help"
        parent = iface.mainWindow()

        click = QAction(icon, text, parent)
        click.triggered.connect(self.help)

        self.toolbarPelagis.addAction(click)
        self.toolbarPelagis.addSeparator()
        self.toolbarPelagis.addAction(self.fct_prepare)
        self.toolbarPelagis.addSeparator()
        self.toolbarPelagis.addAction(self.fct_check)
        self.toolbarPelagis.addSeparator()
        self.toolbarPelagis.addAction(self.fct_linear)
        self.toolbarPelagis.addAction(self.fct_segment)
        self.toolbarPelagis.addSeparator()
        self.toolbarPelagis.addAction(self.fct_analyse)
        self.toolbarPelagis.addAction(self.fct_map)
        self.toolbarPelagis.addSeparator()
        self.toolbarPelagis.addAction(self.fct_integration)
        self.toolbarPelagis.addAction(self.fct_share)


        # connaitre l'état du plugin : A gérer pour ne pas superposer les fenêtre.
        self.pluginEstActif = False  # actif?

    def unload(self):

        self.interface.mainWindow().menuBar().removeAction(self.menuPelagis.menuAction())
        self.interface.mainWindow().removeToolBar(self.toolbarPelagis)

    def controleFenetreOuverte(self, fonctionAOuvrir):

        # pour ne pas ouvrir plusieurs outils en même temps
        for fonction, listeInfo in list(self.dicoFonction.items()):
            if fonction != fonctionAOuvrir:
                if listeInfo[0]:
                    listeInfo[1].close()

    def gereFonction(self, laFonction):

        if self.dicoFonction[laFonction][2]:  # fenêtre dockée
            self.controleFenetreOuverte(laFonction)

            if not self.dicoFonction[laFonction][0]:
                self.dicoFonction[laFonction][0] = True

                if self.dicoFonction[laFonction][1] is None:
                    self.dicoFonction[laFonction][1].fermeFenetreFonction.connect(self.surFermetureFenetreFonction)

                self.interface.addDockWidget(Qt.RightDockWidgetArea, self.dicoFonction[laFonction][1])
                self.dicoFonction[laFonction][1].show()

        else:

            dlg = None
            if laFonction == "fct_prepare":
                dlg = PrepareFileDialog(self.interface)

            if laFonction == "fct_check":
                dlg = CheckDialog(self.interface)

            if laFonction == "fct_linear":
                dlg = LinearDialog(self.interface)

            if laFonction == "fct_segment":
                dlg = SegmentDialog(self.interface)

            if laFonction == "fct_integration":
                dlg = ImportDialog(self.interface)

            if laFonction == "fct_analyse":
                dlg = AnalyseDialog(self.interface)

            if laFonction == "fct_map":
                dlg = MapDialog(self.interface)

            if laFonction == "fct_share":
                dlg = Share_Dialog(self.interface)

            result = dlg.exec_()  # exec_() bloque les autres fenêtres

    def surFermetureFenetreFonction(self, listeFonctionAppelante):

        fonctionAppelante = listeFonctionAppelante[0]

        self.dicoFonction[fonctionAppelante][1].fermeFenetreFonction.disconnect(self.surFermetureFenetreFonction)
        self.dicoFonction[fonctionAppelante][0] = False
        self.dicoFonction[fonctionAppelante][1] = None

# --------------------------------------------------------------------------

    def help(self):

        """
        Called when the user click on the help button.
        """

        showPluginHelp(filename='documentation/build/index')

        # html_doc = os.path.join(os.path.dirname(os.path.realpath(__file__)), "doc/index.html")
        # webbrowser.open('file://' + html_doc)







